# include <stdio.h>

void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int main ()
{
   int foot, inch;
   float height, cent;
   
   while (1){
      printf ("Vvedite rost v formate foot inh\n");
      scanf ("%d %d", &foot, &inch );
	  if (foot>=0 && inch>=0)
	    {
	       cent=(foot*12+inch)*2.54;
		   printf ("Height in centimeters is %.1f\n", cent);
		   break;
 	    }
	  else 
	    {
	       printf("Oshibka vvoda\n");
		   cleanIn();
	    }
   }
   return 0;
}