//Суть алгоритма - перекодировать символы таким образом,
//чтобы наиболее частые символы состояли из меньшего кол-ва бит
//чем более редкие
//В обычной кодировке каждый символ - 8 бит
//Чтобы "читать" символы переменной длины, необходимо, чтобы ни один символ
//не был началом другого т.е. (А=110 и Б=1100 - не допустимо, т.к.дойдя до 3 знака, мы не знаем,
//А это или начало Б
//Алгоритм Хаффмана строит бинарное дерево представления символов оптимальный образом
//Т.е. занимаемая память будет наименьшей, при этом начала всех символов будут уникальны.
//В дереве мы начинаем с вершины(корня). Если двигаемся к левому потомку - добавляем в код 0,
//Если к правому - 1.
//Каждый символ является только листом дерева, у которого нет потомка
//А если нет потомка- значит это уникальное начало.
#include "huffman.h"

int main(int argc, char **argv)
{
	//Таблица символов
	struct SYM table[256];
	//Зануляем
	memset(table,0,sizeof(struct SYM)*256);
	int i;
	int numSym;//Кол-во символов во входном файле
	//инициализируем символы
	for (i=0;i<256;i++)
		table[i].ch=i;
	char *str;	
	//Название входного файла или в параметрах вызова программы, или input.txt
	//Обрабатываем файл
	if (argc==2)
		parse(argv[1],&str,table,&numSym);
	else parse("input.txt",&str,table,&numSym);
	
	printf("num  of symbols: %d\n",numSym);
	
	//Считаем кол-во разных символов ( с freq>0)
	unsigned char Symbols=0;
	for(i=0;i<256;i++)
		if(table[i].freq)
			Symbols++;

	//Делаем бинарное дерево
	struct SYM* root=makeTree(table);
	//Создаем коды для симоволов
	makeCodes(root);
	
	//Выводим на консоль информацию о символах
	for (i=0;i<256;i++)
		if (table[i].freq) 
			printf("%d '%c' freq: %d; code: %s\n",table[i].ch,table[i].ch,table[i].freq,table[i].code);
			
	//Кодируем входной файл
	char *encoded_str;	
	int lng=encode(str,&encoded_str,numSym,table);
	
	//Записываем для отладки полученный код в "encoded.101"
	//Для работы этот блок не нужен, но требуется по заданию
	FILE *enc101;
	enc101=fopen("encoded.101","w");
	fprintf(enc101,"%s",encoded_str);
	fclose(enc101);
	
	//Определяем размер хвоста
	char tail=8-(lng%8);
	printf("\ntail = %d\n",tail);
	
	//Упаковка
	unsigned char *compressed;
	int compLng=compress(encoded_str,lng,tail,&compressed);
	printf("\nleng: %d\n",compLng);
	

	
	//Запись упакованных данных в "compressed.bin"
	FILE *out;
	out=fopen("compressed.bin","wb");
	//Заголовок файла
	short signature=7776; //Сигнатура, чтобы определить, что это наша кодировка
	fwrite(&signature,sizeof(short),1,out);
	fwrite(&Symbols,sizeof(unsigned char),1,out);//Кол-во различных символов
	for(i=0;i<256;i++)
		if(table[i].freq) {//Символ и его частота ( для построения дерева, чтобы разкодировать)
			fwrite(&(table[i].ch),sizeof(char),1,out);
			fwrite(&(table[i].freq),sizeof(int),1,out);
		}
	//Длина хвоста ( в принципе не нужна, но по заданию надо)
	fwrite(&tail,sizeof(char),1,out);
	//Всего кол-во символов
	fwrite(&numSym,sizeof(int),1,out);
	//Сами данные
	fwrite(compressed,sizeof(unsigned char),compLng,out);
	fclose(out);
	
	return 0;
}

