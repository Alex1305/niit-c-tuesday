//Программа для декомпрессии
#include "huffman.h"

int main(int argc, char **argv)
{
	FILE *in;
	in=fopen("compressed.bin","rb");
	//Считываем сигнатуру
	short signature;
	fread(&signature,sizeof(short),1,in);
	//Если сигнатура совпадает  - наш файл
	if (signature!=7776){
		printf("Not our file!");
		return 0;
	}
	else printf("Start decompressing:\n");
	
	//Кол-во различных символов с freq>0
	unsigned char Symbols;
	fread(&Symbols,sizeof(unsigned char),1,in);
	
	//Создаем таблицу символов
	struct SYM table[256];
	memset(table,0,sizeof(struct SYM)*256);
	int i=0;
	for(i=0;i<256;i++)
		table[i].ch=i;
	//Считываем freq для символов с freq>0
	unsigned char chr;
	int frq;
	for(i=0;i<Symbols;i++) {	
		fread(&chr,sizeof(unsigned char),1,in);
		fread(&frq,sizeof(int),1,in);
		table[chr].freq=frq;
	}
	//размер хвоста (не требуется, алгоритм работает без него)
	char tail;
	fread(&tail,sizeof(char),1,in);
	//кол-во символов (требуется для остановки)
	int numSym;
	fread(&numSym,sizeof(int),1,in);
	printf("tail: %d, length: %d \n",tail,numSym);
	
	//Создание дерева (должно совпадать с первой программой)
	struct SYM* root;
	root=makeTree(table);
	
	//Проверка кодов символов - для отладки
	//~ makeCodes(root);
	//~ for(i=0;i<256;i++)
		//~ if(table[i].freq) printf("'%c' %d: %s \n",table[i].ch,table[i].freq,table[i].code);
	
	
	decompress(root,in,numSym);
	fclose(in);
	return 0;
}

