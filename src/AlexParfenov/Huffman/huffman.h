#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Структура представляющая символ
struct SYM {
	unsigned char ch; //Сам символ - например "А"
	int freq; //Кол-во появлений символа в тексте (проще работать с ним, чем с частотой)
	char code[256]; //Код символа (например 1110)
	struct SYM *l; //Указатель на левого потомка
	struct SYM *r; //Указатель на правого потомка
};

//Функция сравнения для квиксорта
//Принимает указатели на указатели Структуры SYM
//Сравнивает частоты. В итоге qsort сортирует частоты в возрастающем порядке
int compare (const void* a,const void* b) {
	struct SYM**aa = (struct SYM**)a;
	struct SYM**bb = (struct SYM**)b;
	if ( (**aa).freq > (**bb).freq ) return 1;
	else return 0;
}

//Проходим по входному файлу
int parse (char *filename, char **pstr, struct SYM arrsym[256], int *numSym) {
	FILE *in;
	in=fopen(filename,"r");
	int size=512;//первоначальный размер массива в который считываем
	char *str = (char *) malloc (sizeof(char)*size);//считываем в str
	short ch;
	int i=0;//Кол-во символов на входе
	while ( (ch = getc(in)) != EOF ) {
		arrsym[ch].freq++;//Увеличиваем частоту символа
		str[i]=ch;//добавляем символ в строку
		if (i==size) { //Если достигли предела размера массива
			size*=2; //Выделяем ему больше места
			str = (char *) realloc(str, sizeof(char) *size);
		}
		i++;
	}
	str[i]='\0';//Добавляем ноль символ в конце
	*pstr=str;//передаем указатель на массив со считаным файлом
	*numSym=i;//Передаем кол-во символов на входе
	fclose(in);
	return 0;
}

//Функция создания бинарного дерева на основе частот символов
struct SYM* makeTree(struct SYM table[256]) {
	//Создаем массив указателей на массив структур SYM
	struct SYM **pSym = malloc( sizeof(struct SYM*)*256 );
	int i;
	for (i=0;i<256;i++) {
		pSym[i]=&(table[i]);//Заполняем адреса в указатели
	}
	//Сортируем указатели по возрастанию частот
	qsort(pSym,256,sizeof(struct SYM*),compare);
	int n=0;
	while (pSym[n]->freq==0) n++;
	//n - кол-во символов с нулевой частотой, пропускаем их
	for (i=n;i<255;i++) {
		//создаем промежуточный узел
		struct SYM *tmp = (struct SYM *) malloc (sizeof(struct SYM));
		memset(tmp,0,sizeof(struct SYM));//инициализируем все поля нулями
		//частота узла будет суммой частот символов с наименьшей частотой (но не нулевых,мы их викинули)
		tmp->freq = pSym[i]->freq + pSym[i+1]->freq;
		//Добавляем в кач-ве потомков эти 2 символа с наименьшей частотой
		tmp->l = pSym[i];
		tmp->r = pSym[i+1];
		//Узел сам становится как бы новым "символом"
		//И процесс продолжается дальше, пока не достигнет корня 
		//для этого надо сделать Nsym-1 слияний, где Nsym- кол-во разных символов с freq>0
		pSym[i+1] = tmp;
		//Снова сортируем оставшийся массив указателей, чтобы сохранить порядок
		qsort(pSym+i+1,256-i-1,sizeof(struct SYM*),compare);
	}
	//возвращаем адрес корня
	return (pSym[255]);
}

//По построенному бинарному дереву, создаем код для каждого символа
//Начиная с корня
void makeCodes(struct SYM* root) {
	//Если есть левый потомок, создаем его код из кода родителя +"0"
	if ( root->l) {
		strcpy(root->l->code,root->code);
		strcat(root->l->code,"0");
		makeCodes(root->l);
	}
	//Если есть правый -код родителя + "1" 
	if (root->r ) {
		strcpy(root->r->code,root->code);
		strcat(root->r->code,"1");
		makeCodes(root->r);
	}
}

//Кодируем исходный текст по полученным кодам
int encode(char *str,char **encoded, int n, struct SYM *table) {
	int i=0;
	int size=n*8;//размер в среднем не должен превосходить кол-во символов * 8
	*encoded = (char*) malloc(sizeof(char)*size);
	(*encoded)[0]=0;
	int length=0;//Кол-во символов в закодированной послед
	while(str[i]) {
			strcat(*encoded,table[(int)str[i]].code);//Добавляем в строку новый код символа
			length+=strlen(table[(int)str[i]].code);
			if (i==size-256) { //256 макс возможный символ, если есть угроза переполнения, увеличиваем размер строку
				size=2*(size+256);
				*encoded=realloc(encoded,size);
			}
			i++;
			
	}
	(*encoded)[length]=0;
	return length;
}


//Компрессия(Упаковка)
//Каждые 8 символов ('1' и '0') превращаем в 1 байт
int compress(char* encoded,int length,int tail,unsigned char **compressed) {
	int i=0;
	int bit=0;
	unsigned char byte;
	int comprLeng=0;
	int size = (length + tail+8)/8;
	//выделяем место под сжатую строку
	*compressed =(unsigned char *) malloc(sizeof(unsigned char) * size);
	//обнуляем ее
	memset(*compressed,0,sizeof(unsigned char) * size);
	while(encoded[i]) {
		if (bit<8) { //Пока не достигли 8 бит
			byte=byte<<1;//Сдвигаем байт влево на 1 бит
			byte=byte | (encoded[i]-'0'); //Заполняем правый бит 1 или 0 (в зависимости что в ecncoded)
			bit++; //Увеличиваем номер полученного бита
		}
		if (bit==8) { //Если мы собрали байт
			(*compressed)[comprLeng]=byte; //записываем его в строку
			bit=0; //Обновляем счетчики
			byte=0;
			comprLeng++;
		}
		i++;
	}
	//Проверяем хвост
	if (bit) { //если последних бит не хватает, чтобы собрать байт (т.е. остаются bit>0)
		//записываем этот недоделанный байт в строку
		(*compressed)[comprLeng]=byte;
		(*compressed)[comprLeng]=(*compressed)[comprLeng]<<tail;//Сдвигаем влево на велечину хвоста
		comprLeng++;
	}
	(*compressed)[comprLeng]=0;
	return comprLeng;
	return 0;
} 
	
//Декомпрессия
int decompress(struct SYM* root, FILE *in, int total) {
	FILE *out;
	out=fopen("decompressed.txt","w");
	unsigned char byte;
	unsigned char bit;
	int c;
	struct SYM *leaf=root; //Начальной положение - корень
	int numDecomp=0;//Кол-во декомпрессированных символов (должно быть равно total - всего симв из 1 программы)
	while ( (byte=fgetc(in) ) !=EOF ){ //Считываем по байту до конца программы
		c=0;//счетчик обработаных битов из байта
		while (c<8) {
			if ((leaf->r==0) && (leaf->l==0)) { //Если достигли листа (нет потомков), то это адрес символа
				fputc(leaf->ch,out);  //Добавляем символ в файл
				numDecomp++;//увеличиваем счетчик символов
				if (numDecomp==total) {//Если достигли последнего символа, завершаем функцию
					fclose(out);
					return numDecomp;
				}
				else leaf=root;//Возвращем адрес на корень,чтобы считывать новый символ
			}
			//128 == 2^7 == 10000000 в двоичном представлении
			// Т.к нам нужен старший бит XXXXXXXX & 10000000 == X0000000 
			bit = byte & 128; //получаем левый бит из байта
			byte=byte<<1; //Сдвигаем байт влево на 1 бит ( 11001000 << 1 == 10010000)
			c++; //Увеличиваем счетчик считанных битов
			if (bit==0) leaf=leaf->l; //Если бит ==0 то двигаемся к левому потомку
			else leaf=leaf->r; //Если бит !=0 (==128 в нашем случае), то двигаемся к правому листу
		}
	}
	fclose(out);
	return 0;
}

