/*6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
 * 
 */


#include <stdio.h>

int main()
{
	int n;
    int i;
	char table[100][30];
	char *young,*old;
	int y=100000;
	int o=-1;
	int cur;
	printf("Input the number of family's members: ");
	scanf("%d",&n);
	printf("Input name and age for every n members:\n");
	for (i=0;i<n;i++) {
		scanf("%s %d",table[i],&cur);
		if (cur<y) {
			young=table[i];
			y=cur;
		}
		if (cur>o) {
			old=table[i];
			o=cur;
		}
	}
	if (n>0) {
		printf("Oldest %d %s\n",o,old);
		printf("Youngest %d %s\n",y,young);
	}
	else printf("There is no members\n");
	getc(stdin);
	return 0;
}