/*3. �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������:
���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������
*/

#include <stdio.h>
#include <string.h>
int main()
{
	char str[512];
    char *begin;
    char *end;
	printf("Input the line:\n");
	fgets(str,512,stdin);
	begin=str;
	end=str+strlen(str);
	while (*end=='\n' || *end=='\0' || *end=='\r') 	
		end--; 										
	while (begin<end) { 
		if (*begin!=*end) { 
			printf("Not palindrome\n"); 
			getc(stdin);
			return 0;
		}
		begin++;
		end--;
	}
	printf("Palindrome!\n");
	getc(stdin); 
	return 0;
}

