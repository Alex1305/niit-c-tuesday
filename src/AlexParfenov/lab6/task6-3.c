﻿/*3. Написать программу, которая переводит введённое пользователем
целое число в строку с использованием рекурсии и без каких-либо
библиотечных функций преобразования
 */
 
#include <stdio.h>
#include <stdlib.h>

char* toStr(int n,char *str,int *i) {
	
	if (n<10) {
		//то записываем его в начало строки
		//'0' -код символа '0' + сдвиг дает код нового числа 
		//например '0'+ 2 == '2'
		str[0]= '0' + n;
		(*i)++;//увеличиваем индекс 
		return str;
	}
	else {
		
		char d = '0' + (n%10);
		toStr(n/10,str,i);
		str[*i]=d;
		(*i)++;
		str[*i]='\0';
		return str;
	}
}	

int main(int argc, char **argv)
{
	char *str=malloc (sizeof(char)*100);
	printf("Input a integer: ");
	int n;
	scanf("%d",&n);//считываем int
	int k=0;
	str= toStr(n,str,&k);//посылаем int,получаем строку
	printf("String repr: %s",str);
	return 0;
}


