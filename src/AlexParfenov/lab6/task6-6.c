/*6. Написать реализацию рекурсивной функции, вычисляющую n-ый
элемент ряда Фибоначчи, но без экспоненциально растущей рекур-
сии
Замечание:
Нужно создать две функции: одна вызывается непосредственно из main и вы-
зывает вторую, вспомогательную, которая и является рекурсивной.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#define MAX_N 46
//СМ. описание в пред задании
int recursiveFib(int n,int *table) {
	if (table[n]!=-1) return table[n];
	table[n]=recursiveFib(n-1,table)+recursiveFib(n-2,table);
	return table[n];
}

int fib(int n) {
	//выделяем память под таблицу
	int *table=malloc(sizeof(int)*(n+1));
	int i=0;
	//инициализируем таблицу
	for (i=0;i<=n;i++) table[i]=-1;
	table[0]=0;table[1]=1;table[2]=1;
	int F=recursiveFib(n,table);//получаем необходимое число
	free(table);//освобождаем память
	return F;
}
	
int main(int argc, char **argv)
{
	printf("Input the n-th fib to calc: ");
	int n;
	scanf("%d",&n);
	printf("The %d Fibonacci is : %d",n,fib(n));
	getc(stdin);
	return 0;
}

