﻿/*1. Написать программу, которая формирует в двумерном символьном
массиве фрактальное изображение и выводит его на консоль (см.
рисунок)
* Фракталом обычно называют изображение, любая часть которого подобна це-
лому. Поэтому, при рисовании фрактала используют рекурсию и некоторый
масштабный коэффициент, определяющий размер изображения.
* 
*/
#include <stdio.h>
#include <stdlib.h>

int power(int base,int power) {
	int r=1;
	int i=0;
	while (i<power) {
		r*=base;
		i++;
	}
	return r;
}

char * fractal (char *grid,int n, int finish) {
	
	if (n==finish+1) {
		return grid;
	}
	if (n==0) {
		return fractal(grid,n+1,finish);
	}
	else {
		//size - размер нового фрактала
		//psize - размер фрактала на входе
		int size,psize;
		size=power(3,n); //Размер - степень тройки 
		psize=power(3,n-1);//т.е для n=1 3^0 1х1 текущий шаблон, 3^1: 3x3 фрактал на выходе, 3^2 9x9 след фрактал...
		char *out =  malloc (sizeof (char) * size *size );
		int i,j;
		//Заполняем пробелами новый фрактал
		for(i=0;i<size*size;i++)out[i]=' ';
		for (i=0;i<psize;i++)
			for(j=0;j<psize;j++)  {
				//Формируем фрактал по шаблону
				//Самоподобие - т.е. крест рисуется целиком шаблоном
				//       *
				// * -> *** -> дальше этим крестом рисуем новый крест ->
				//		 *	
				out[i*size + (j+psize) ] =grid[i*psize + j];
				out[ (i+psize)*size + j] =grid[i*psize + j];
				out[ (i+psize)*size + (j+psize)   ] =grid[i*psize + j];
				out[ (i+psize)*size + (j+psize*2) ] =grid[i*psize + j];
				out[ (i+psize*2)*size + (j+psize) ] =grid[i*psize + j];
		}
		free(grid);//освобождаем память от старого шаблона
		//посылаем новый фрактал в качестве шаблона, для нового фрактала
		//размерности n+1
		return fractal(out,n+1,finish);
		//функция вернет указатель на адрес фрактала до самого верха по рекурсии
	}
	return NULL;
}

int main(int argc, char **argv)
{
	printf("Input fractal scale: ");
	int n;
	scanf("%d",&n);
	char *grid = malloc(sizeof(char));
	*grid='*';
	grid=fractal(grid,0,n);
	int N,i,j;
	N=power(3,n);
	for(i=0;i<N;i++) {
		for(j=0;j<N;j++)
			printf("%c",grid[i*N+j]);
		printf("\n");
	}
	getc(stdin);
	return 0;
}

