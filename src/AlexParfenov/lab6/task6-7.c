﻿/*Написать программу, которая находит выход из лабиринта
Замечание:
(a) лабиринт задаётся в виде двумерного символьного массива;
(b) начальная позиция - в центре;
(c) программа определяет (по символу), что находится вокруг текущей ячей-
ки;
(d) если ячейка свободна, программа перемещается в данную точку и всё
повторяется;
(e) программа узнаёт о выходе из лабиринта при пересечении его внешней
границы.
Замечание:
Символом ’#’ обозначены стены лабиринта
Символом ПРОБЕЛ - свободный проход
Символом ’X’ - местоположение человека
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//Фукция для копирования массива со статусами
char* copyStatus(char *status, int n,int m) {
	//Выделяем память для копии
	char *copy= malloc (sizeof(char)*n*m);
	//Копируем память из адреса status, в copy, размером sizeof...
	memcpy(copy,status,sizeof(char)*n*m);
	return copy;
}
//Проверка, достигли ли границы
int isExit(int i,int j,int n,int m) {
	if (j==0 || j==m-1 || i==0 || i==n-1) return 1;
	return 0;
}
//печать лабиринта
int printLab (char str[][256],int n) {
	int i=0;
	while(i<n) {
		printf("%s",str[i]);
		i++;
	}
	return 0;
}
//Определение следующей позиции
//dir - направлние, 0-вверх, 1-вправо
//2-вниз, 3-влево.
//Направление сохраняется, пока не встретится препятствие
int nextPos(int *i,int *j,int *dir) {
	*dir=*dir%4;
	if (*dir==0) {(*i)--; return 0;}
	if (*dir==1) {(*j)++;return 0;}
	if (*dir==2) {(*i)++;return 0;}
	if (*dir==3) {(*j)--;return 0;}
	return 0;
}
//Сканируем текущее положение
//На входе str- лабиринт (2 мерный массив)
//i,j указатели на адреса, содержащие текущее положение
//n.m -размер лабиринта n- высота,m - ширина
//dir - направление
//status - массив статус (1 - клетка, в которой были, 0 - не были)
int scan(char str[][256],int *i, int *j,int n,int m,int *dir,char *status) {
	
	if (isExit(*i,*j,n,m)) 
		return 1;
	int *ii=malloc(sizeof(int));
	int *jj=malloc(sizeof(int));
	int d=*dir;//копируем направление 
	int k=0;
	char *copStat=copyStatus(status,n,m);
	while (k<4) { 
		(*dir) = (d + k) % 4; 
		*ii=*i;*jj=*j;
		nextPos(ii,jj,dir);
		
		if (str[*ii][*jj] == ' ' && status[(*ii)*m+(*jj)]==0 ) {
		
			copStat[(*ii)*m+*jj]=1; 
			//По рекурсии отправляемся вглубь для этого шага
			if (scan(str,ii,jj,n,m,dir,copStat)) { 
				free(copStat);
				str[*ii][*jj]='X';
				printLab(str,n);
				free(ii);
				free(jj);
				printf("Press button to keep tracing\n");
				getc(stdin);
				return 1;
			}
			else
				
				copStat[(*ii)*m+*jj]=0;
		}
		k++;	
		
	}
	free(copStat);
	free(ii);
	free(jj);
	//0 возвращаем, если выход не найден 
	return 0; //по всем 4 направлениям некуда идти (или стена, или статус 1 - уже были)
}

int main(int argc, char **argv)
{
	FILE *in;
	in=fopen("labirint.txt","r");//открываем файл с лабиринтом
	char str[256][256];
	int i,j;
	int n,m;
	i=0;
	while (fgets(str[i],256,in)) {//считываем лабиринт в str
		i++;
	}
	n=i;
	i=0;
	while (str[0][i]=='#' || str[0][i]==' ' || str[0][i]=='X') i++;
	m=i;
	char *status = calloc(n*m,sizeof(int));
	for (i=0;i<n;i++)
		for(j=0;j<m;j++)
			if(str[i][j]=='X') {//ищем в лабиринте нашу стартовую точку
				int *dir=malloc(sizeof(int));//выделяем память под направление
				status[i*m+j]=1;
				scan(str,&i,&j,n,m,dir,status);//запускаем поиск
				getc(stdin);
				free(dir);
				free(status);
				return 0;
			}
	return 0;
}


