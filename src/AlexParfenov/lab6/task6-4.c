﻿/*4. Написать программу, которая суммирует массив традиционным и
рекурсивным способами
Замечание:
Программа выполняет следующую последовательность действий:
(a) принимает из командной строки значение степени двойки M;
(b) находит размер динамического массива N = 2 M ;
(c) выделяет память под динамический массив;
(d) случайным образом заполняет массив данными;
(e) находит сумму традиционным и рекурсивыным способом;
(f) сравнивает время выполнения суммирования трпдиционным и рекурсив-
ным способом;
(g) освобождает динамическую память
 * 
 */


#include <stdio.h>
#include <time.h>
#include <stdlib.h>

//Функция возведения в степень
int power(int base,int power) {
	int r=1;
	int i=0;
	while (i<power) {
		r*=base;
		i++;
	}
	return r;
}

int sum(int *a,int i,int n) {
	if(n-i==2) return a[i]+a[i+1];
	else 
		return sum(a,i,(i+n)/2) + sum(a,(i+n)/2,n);
}
int main(int argc, char **argv)
{
	printf("Input power M for array's size 2^M : ");
	int m;
	scanf("%d",&m);
	int n=power(2,m);
	printf("%d\n",n);
	int *a = malloc (sizeof(int)*n);
	srand(time(NULL));
	int i;
	clock_t begin_t,end_t;
	for(i=0;i<n;i++) {
		a[i]=rand()%1000;
	}
	begin_t=clock();
	int s=0;
	for(i=0;i<n;i++)
		s+=a[i];
	end_t=clock();
	double diff =(double) (end_t - begin_t)/CLOCKS_PER_SEC;
	printf("\n%d, calculation time : %f",s,diff);
	begin_t=clock();
	s=sum(a,0,n); 
	end_t=clock();
	diff = (double)(end_t - begin_t)/CLOCKS_PER_SEC;
	printf("\n%d, calc time : %f",s,diff);
	free(a);
	getc(stdin);	
	return 0;
}

