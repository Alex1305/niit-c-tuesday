﻿/*5. Написать программу, которая измеряет время вычисления N-ого
члена ряда Фибоначчи. Предусмотреть вывод таблицы значений
для N в диапазоне от 1 до 40 (или в другом диапазоне по желанию)
на экран и в файл
Замечание:
Текстовый файл со значениями можно открыть в электронной таблице и по-
строить график зависимости времени от члена ряда N
 */


#include <stdio.h>
#include <time.h>

#define MAX_N 46
int table[MAX_N];

int fib(int n) {
	
	if (table[n]!=-1) return table[n];
	table[n]=fib(n-1)+fib(n-2);
	return table[n];
}

int main(int argc, char **argv)
{
	int i=0;
	for(i=0;i<MAX_N;i++)table[i]=-1;
	table[0]=0;table[1]=1;table[2]=1;
	int n;
	printf("Input the N number of Fib to calc: ");
	scanf("%d",&n);
	if (n>MAX_N) printf("Error: too big n\n");
	else {
		clock_t b,e;
		b=clock();
		fib(n);
		e=clock();
		double t = (double) (e-b)/CLOCKS_PER_SEC;
		printf("time of calculating %d-th fib number: %f",n,t);
		FILE *out;
		out=fopen("fib.txt","w");
		for (i=1;i<n+1;i++) {
			printf("%d,%d\n",i,table[i]);
			fprintf(out,"%d,%d\n",i,table[i]);
		}
		fclose(out);
	}
	getc(stdin);
	return 0;
}


