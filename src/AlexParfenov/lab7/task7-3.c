/*3. Написать программу, которая строит таблицу встречаемости сим-
волов для произвольного файла, имя которого задаётся в команд-
ной строке. Программа должна выводить на экран таблицу встре-
чаемости, отсортированную по убыванию частоты
Замечание:
В программе необходимо определить структурный тип SYM, в котором нужно
хранить код символа и частоту встречаемости (вещественное число от 0 до 1).
После анализа файла, массив структур SYM должен быть отсортирован по
частоте.
 */
//Структура хранящая код символа и его частоту
struct SYM {
	unsigned char sym;
	double frq;
};

#include <stdio.h>
#define CHARS 256 //можно на самом деле 128, т.к. вторую половину все равно не печатаем

int main(int argc, char **argv)
{
	struct SYM table[CHARS];//массив структур для каждого символа
	int i;
	for(i=0;i<CHARS;i++) {//инициализируем
		table[i].sym=i;//индекс массива на данном этапе совпадает с кодом ASCII символа
		table[i].frq=0;
	}
	FILE *in;
	printf("Input file name: ");
	char filename[50];
	scanf("%s",filename);//Считываем имя файла для анализа
	in=fopen(filename,"r");
	char line[256];
	int n;
	while (fgets(line,256,in)) {//Считываем строки файла
		i=0;
		while (line[i]) {
			table[ (unsigned char)line[i] ].frq++;//увеличиваем частоту встретившегося символа
			i++;
			n++;
		}
	}
	int j;
	struct SYM tmp;
	for (i=0;i<CHARS-1;i++)
		for(j=i+1;j<CHARS;j++)
			if(table[i].frq<table[j].frq) {//сортируем символы по частоте
				tmp=table[i];
				table[i]=table[j];
				table[j]=tmp;
			}
	//Печатаем символы и частоту (в порядке убывания частоты)
	for (i=0;i<CHARS;i++)
			if (table[i].frq && table[i].sym>31 && table[i].sym<128)
				printf("%c : %.3f%%\n",table[i].sym,table[i].frq*100/n);
	getc(stdin);
	return 0;
}

