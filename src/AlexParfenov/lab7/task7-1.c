﻿/*1. Написать программу, создающую связанный список с записями о
регионах и их кодах в соответствии с содержанием файла данных
Замечание:
Файл скачивается по адресу: http://introcs.cs.princeton.edu/java/data/fips10_4.csv
Программа должна поддерживать следующие функции:
(a) Формирование списка на основе данных файла.
(b) Поиск и вывод всех данных по буквенному обозначению страны.
(c) Поиск конкретного региона по названию.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
//Структура содержащие данные о регионе
struct region{
	char country[3];//Буквенный код - 2 буквы + \0
	char code[3];// Числовой код - 2 цифры + \0
	char name[50];//Имя формата "Название"
};
//Из данных выше образуем связный лист
struct node{
	struct region data;
	struct node *next;
};
//Функция формирования листа из файла .csv
struct node* formList(FILE *in) {
	char str[100];
	//Объявляем первый элемент листа,
	struct node *top=NULL;//и поскольку лист пуст, присваиваем 0 адрес
	fgets(str,100,in);//пропускаем первую линию
	while ( fgets(str,100,in) ) {
		struct node *item = malloc( sizeof(struct node)); 
		item->next=top;
		//копируем данные в соответствующие поля и добавляем терм символ '\0' где необходимо
		strncpy(item->data.country,str,2);item->data.country[2]='\0';
		strncpy(item->data.code,str+3,2);item->data.code[2]='\0';
		strcpy(item->data.name,str+6);
		top=item;
	}
	return top;
}
//Функция ищет и выводит совпадения буквенного кода (RU)		
void searchCountry(char *find,struct node*list) {
	//find - строка для поиска
	find[0]=toupper(find[0]);
	find[1]=toupper(find[1]);
	while (list->next!=NULL) { //Проходим по листу с вершины до конца
		//strncmp(s1,s2,n) возвращает 0, если первые n символов строк s1 и s2 равны
		if (strncmp(list->data.country,find,2)==0) 
			printf("%s,%s,%s",list->data.country,list->data.code,list->data.name);
		list=list->next;//Двигаемся дальше по списку
	}
}
//Функция поиска региона по названию
void searchRegion(char *find,struct node*list) {
	find[1]=toupper(find[1]);//делаем первую букву заглавной
	while (list->next!=NULL) {//двигаемся по листу до конца
		if (strncmp(list->data.name,find,strlen(find))==0) //если значение имени совпадает
			printf("%s,%s,%s",list->data.country,list->data.code,list->data.name);//печатаем
		list=list->next;//переход к след элементу листа
	}
}
//Освобождение памяти
void destroyList(struct node*list) {
	struct node*tmp;
	while (list->next) {
		tmp=list->next;
		free(list);
		list=tmp;
	}
}
int main(int argc, char **argv)
{
	FILE *in;
	in=fopen("fips10_4.csv","r");
	struct node *list;
	list = formList(in);//Формирование листа, на основе данных файла
	printf("Input ALPHA CODE (2 letters - RU) to find: ");
	char find[3];
	scanf("%s",find);
	searchCountry(find,list);//поиск по коду
	printf("Input Name of region to find: ");
	char reg[50];
	reg[0]='\"';
	scanf("%s",reg+1);
	searchRegion(reg,list);//поиск по имени
	getc(stdin);
	fclose(in);//закрываем файл
	destroyList (list);//освобождаем память
	return 0;
}


