﻿/*1. Написать программу, которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.
Замечание:
Программа должна состоять минимум из трех функций:
a) printWord - выводит слово из строки (до конца строки или пробела)
b) getWords - заполняет массив указателей адресами первых букв слов
c) main - основная функция
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>


int printWord(char *ptr) {
	
	while ( isalpha(*ptr) || isdigit(*ptr) ) {
		printf("%c",*ptr);
		ptr++;
	}
	return 0;
}

int getWords (char *str, char **ptr, int *n) {
	int i=0;
	*n=0;
	while ( str[i] ) {
		if ( isalpha(str[i]) || isdigit(str[i]) ) { 
			ptr[*n]=&(str[i]); 
			while ( isalpha(str[i]) || isdigit(str[i]) )
				i++;
			(*n)++;	
		}
		else i++;
	}
	return 0;	
}	

int shuffle (char **source_array, int n, char **out_array) {
	srand(time(NULL));
	unsigned char isShuffled[n];
	memset(isShuffled, 0 , sizeof(unsigned char) * n);
	int i;
	int j;
	for (i=0;i<n;i++) {
		j=rand()%n;
		while ( isShuffled[j]== 1)  
			j = (j+1)%n;
		isShuffled[j]=1;
		out_array[i]=source_array[j];
	}	
	return 0;
}
int parseLine(char *str) {
	char *words[50];
	int n;
	getWords(str,words,&n);
	int i;
	char *shuffled_words[50];
	shuffle(words,n,shuffled_words);
	for (i=0;i<n;i++) {
		printWord(shuffled_words[i]);
		printf(" ");
	}
	printf("\n");
	return 0;
}

int main(int argc, char **argv)
{
	printf("Input line:\n");
	char str[256];
	fgets(str,256,stdin);
	parseLine(str);
	getc(stdin);
	return 0;
}

