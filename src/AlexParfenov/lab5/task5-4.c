﻿/*4. Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки вызывается функция, разработанная в рамках задачи 1.
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

int printWord(char *ptr) {
	while ( isalpha(*ptr) || isdigit(*ptr) ) {
		printf("%c",*ptr);
		ptr++;
	}
	return 0;
}

int getWords (char *str, char **ptr, int *n) {
	int i=0;
	*n=0;
	while ( str[i] ) {
		if ( isalpha(str[i]) || isdigit(str[i]) ) {
			ptr[*n]=&(str[i]);
			while ( isalpha(str[i]) || isdigit(str[i]) )
				i++;
			(*n)++;	
		}
		else i++;
	}
	return 0;	
}	

int shuffle (char **source_array, int n, char **out_array) {
	srand(time(NULL));
	unsigned char isShuffled[n];
	memset(isShuffled, 0 , sizeof(unsigned char) * n);
	int i;
	int j;
	for (i=0;i<n;i++) {
		j=rand()%n;
		while ( isShuffled[j]== 1) 
			j = (j+1)%n;
		isShuffled[j]=1;
		out_array[i]=source_array[j];
	}	
	return 0;
}

int ParseLine(char *str) {
	char *words[50];
	int n;
	getWords(str,words,&n);
	int i;
	char *shuffled_words[50];
	shuffle(words,n,shuffled_words);
	for (i=0;i<n;i++) {
		printWord(shuffled_words[i]);
		printf(" ");
	}
	printf("\n");
	return 0;
}

int main(int argc, char **argv)
{
	FILE *in;//указатель на файл
	in=fopen("input.txt","r");//открываем на чтение
	char str[256];
	while (fgets(str,256,in))//считываем строки до конца файла
		ParseLine(str);//обрабатываем
	fclose(in);
	getc(stdin);
	return 0;
}


