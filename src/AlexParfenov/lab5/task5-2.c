﻿/*2. Написать программу ”Калейдоскоп”, выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек ’*’.
Изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
Замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение
’*’)
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка
7) Переход к шагу 1.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> 

#define N 10 
int clearQuad (char quad[][N]) {
	int i,j;
	for (i=0;i<N;i++)
		for(j=0;j<N;j++)
			quad[i][j]=' ';
	return 0;
}

int formQuad (char quad[][N]) {
	int i,j;
	for (i=0;i<N;i++)
		for (j=0; j<N;j++)
			if (rand()%2) quad[i][j]='*';
	return 0;
}

int copyToScreen (char quad[][N], char screen[][2*N]) {
	int i,j;
	for (i=0;i<N;i++)
		for(j=0;j<N;j++) {
			screen[i][j]=quad[i][j];
			screen[i][j+N]=quad[i][N-1-j];
			screen[i+N][j]=quad[N-1-i][j];
			screen[i+N][j+N]=quad[N-1-i][N-1-j];
		}
	return 0;
}	

int clearConsole () {
	int i=0;
	for (i=0;i<4*N;i++) printf("\n");
	return 0;
}

int printScreen ( char screen[][2*N]) {
	int i,j;
	for (i=0;i<2*N;i++) {
		for(j=0;j<2*N;j++)
			printf("%c",screen[i][j]);
		printf("\n");
	}
	return 0;
}

int main(int argc, char **argv)
{
	char quad[N][N];
	char screen[N*2][N*2];
	srand(time(NULL));
	while (1) { 
		clearQuad(quad);
		formQuad(quad);
		copyToScreen(quad,screen);
		clearConsole();
		printScreen(screen);
		sleep(5);
	}
	return 0;
}


