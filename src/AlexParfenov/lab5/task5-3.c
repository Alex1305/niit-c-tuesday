﻿/*3. Написать программу, переставляющую случайным образом симво-
лы каждого слова каждой строки текстового файла, кроме первого
и последнего, то есть начало и конец слова меняться не должны.
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки выполняется разбивка на слова и независимая обра-
ботка каждого слова.
 * 
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

int shuffle (char *begin, char *end) {
	if (end-begin<=3) return 0;
	begin++;
	end--;
	int n=end-begin;
	char *buffer = malloc (sizeof(char)*n);
	char *isShuffled = calloc (0,sizeof(char)*n);
	int i,j;
	for (i=0;i<n;i++) {
		j=rand()%n;
		while ( isShuffled[j] ) 
			j= (j+1)%n; 
		isShuffled[j]=1;
		buffer[i]=*(begin+j);
	}
	for (i=0; i<n;i++)
		begin[i]=buffer[i];
	free (buffer);
	free (isShuffled);
	return 0;
}

int parseLine(char *str) {
	int i=0;
	char *begin;
	char *end;
	while (str[i]) {
		if( isalpha(str[i]) || isdigit(str[i]) ) {
			begin=str+i;
			while ( isalpha(str[i]) || isdigit(str[i]) ) 
				i++;
			end=str+i;
			shuffle(begin,end);
		}
		else i++;
	}
	return 0;
}

int main(int argc, char **argv)
{
	FILE *in,*out;
	in=fopen("input.txt","r");//открываем на чтение
	out=fopen("output.txt","w");
	char str[256];
	int i=0;
	srand(time(NULL));
	while ( fgets(str,256, in) ) {
		i++;
		printf("%s",str);
		parseLine(str);
		printf("%s",str);
		fprintf(out,"%s",str);
	}
	fclose(in);//закрываем файлы
	fclose(out);
	return 0;
}


