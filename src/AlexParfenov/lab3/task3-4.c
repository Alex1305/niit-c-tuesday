/*4. �������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.
*
* max digits in number == 8
*/

#define DIGITS 8


#include <stdio.h>
#include <ctype.h>
#include <string.h>
int main()
{
	int n;
    char str[1025];
	int s=0;
	int i=0;
	int j;
	printf("Input the line:\n");
	fgets(str,1025,stdin);
	while (str[i]) {
		if (isdigit (str[i])) {
			j=i;
			while (isdigit (str[i]) && i-j<DIGITS)
				i++;
			sscanf(str+j,"%8d ",&n);
			s+=n;
		}
		else
			i++;
	}
	printf("sum of numbers with max digits %d = %d\n",DIGITS,s);
	getc(stdin);
	return 0;
}