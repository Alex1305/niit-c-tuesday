/*10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������


 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	char str[1025];
	int n;
	int i=0;
	int k=0;
	int j=0;
	printf("Input line:\n");
	fgets(str,1025,stdin);
	printf("Input n:\n");
	scanf("%d",&n);
	if (n<1) {
		printf("Error : Wrong n\n");
		return 1;
	}
	while (str[i]) {
		if ( isalpha(str[i])) { 
			k++;
			j=i;
			while (isalpha(str[i]))
				i++;
			if(k==n) { 
				j--;
				while(str[i]) { 
					str[j]=str[i]; 
					j++;
					i++;
				}
				str[j]='\0';
				printf("%s\n",str);
				getc(stdin);
				return 0;
			}
		}
		else 
			i++;
	}
	printf("Error : Wrong n\n");
	getc(stdin);
	return 1;
}
