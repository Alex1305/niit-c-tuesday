/*5. �������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)


*/
#include <stdio.h>
#include <stdlib.h> 
#include <time.h> 
#define MAX_NUM 1000
#define N 128
int main()
{
	int a[N];
    int n;
	int i;
	int flag=0;
	int sum=0;
	int buff=0;
	printf("Input N :\n");
	scanf("%d",&n);
	if (n>N) 
		while(n>N) {
			printf("Error: n is too big\nInput new n: ");
			scanf("%d",&n);
		}
	srand(time(NULL));
	for (i=0;i<n;i++) {

		a[i]=rand()%MAX_NUM - rand()%MAX_NUM;
		printf("%d ",a[i]);
		if (!flag && a[i]<0) {
			flag=1;
			continue;
		}
		if (flag) { 
			if (a[i]>0) { 
				sum+=buff;
				buff=0;
			}
			buff+=a[i];
		}
	}
	printf("\n%d sum between first negative and last positive",sum);
	getc(stdin);
	return 0;
}